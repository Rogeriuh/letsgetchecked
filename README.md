# LetsGetChecked

Repo with QA Engineer - Technical Challenge

## Installation and Run

Used Maven and Cucumber

```bash
mvn clean install -Dcucumber.option="--tags @All" -q
```

## Cucumber Features Files

Here you can check all Gherkin

```bash
features/MapsTest.feature
```

## Java Files with test

This is where tests are implemented

```bash
src/test/java/stepDefinitions/MapsTest.java
```

## Cucumber Configs

Responsible to setting test run to Cucumber and link features folder with stepDefinitions folder
This allows Cucumber to know where his Gherkin is defined as test

```bash
src/test/java/runner/CukesRunnerTest.java
```



Thanks,
Rogério Castro