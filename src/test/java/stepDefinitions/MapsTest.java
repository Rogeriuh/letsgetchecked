package stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.Keys; // only needed if sendkeys(Keys.RETURN)

import cucumber.api.java.en.Given;

public class MapsTest {
	
	static WebDriver driver;

	@Given("^Search google maps for Dublin$") // this is because I used Cucumber as BDD framework
	public void visitGoogleMaps() throws Exception{
		
		// set path to ChromeDriver, please change it to you path in order to work
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		
		// create an instance of ChromeDriver and maximize it
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		// visit google maps
		driver.get("https://www.google.com/maps");

		// don't know if this was expected but google asks for consent when opening with no account logged (redirected to consent.google.com before going to maps)
		WebElement consentPage = driver.findElement(By.cssSelector("button[aria-label]"));

		consentPage.click();
			
		// create WebElements top interact (great for a PageObjects approach)
		WebElement searchbox = driver.findElement(By.id("searchboxinput"));
		WebElement magnifyingGlassSearchButton = driver.findElement(By.id("searchbox-searchbutton"));

		//search
		final String city = "Dublin";
			
		searchbox.sendKeys(city);
		magnifyingGlassSearchButton.click(); //could also use magnifyingGlassSearchButton.sendKeys(Keys.RETURN)

		// wait page to load so that selenium can find the element (yes, it takes some time to get the left Panel)
		Thread.sleep(5000);

		// validate if text on element is the same as searched
		WebElement leftPanelHeader = driver.findElement(By.cssSelector("div#pane h1"));
		Assert.assertEquals(leftPanelHeader.getText(),city);
				
		// Usually would go for selector "div#pane button[data-value='Direções']" but realized that elements like data-value or arial-label
		//		might change depending on language so decided to get a more maintainable approach.
		WebElement directions = driver.findElement(By.cssSelector("div#pane button img[src='//www.gstatic.com/images/icons/material/system/1x/directions_white_18dp.png']"));
		directions.click();

		// wait page to load so that selenium can find the element
		Thread.sleep(1000);
				
		// validated if ID would chance with the search, because it seems like autogenereted, but realized it is always the same.
		WebElement destinationField = driver.findElement(By.cssSelector("#sb_ifc52 > input"));
		final String DestinationValue = destinationField.getAttribute("aria-label");

		Assert.assertTrue(DestinationValue.contains(city));

		// quit driver to close browser before finishing tests
		driver.quit();
	
	}	
}
